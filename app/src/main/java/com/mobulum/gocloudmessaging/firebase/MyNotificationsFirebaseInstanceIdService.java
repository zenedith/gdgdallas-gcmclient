package com.mobulum.gocloudmessaging.firebase;

import android.os.AsyncTask;
import android.os.Build;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import timber.log.Timber;

public class MyNotificationsFirebaseInstanceIdService extends FirebaseInstanceIdService {
    public static final String BBQL_LOCALIZATION_TOPIC = "bbq";
    private static final String API_URL = "https://bbq-fcm-message.appspot.com/regsave";

    /**
     * The Application's current Instance ID token is no longer valid and thus a new one must be requested.
     */
    @Override
    public void onTokenRefresh() {
        // If you need to handle the generation of a token, initially or after a refresh this is
        // where you should do that.
        String token = FirebaseInstanceId.getInstance().getToken();
        Timber.d("FCM Token: %s", token);

        // Once a token is generated, we subscribe to topic.
//        FirebaseMessaging.getInstance().subscribeToTopic(BBQL_LOCALIZATION_TOPIC);
        FirebaseMessaging.getInstance().subscribeToTopic("/topics/" + BBQL_LOCALIZATION_TOPIC);


        sendRegId(token);
    }

    private void sendRegId(final String regId) {
        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... params) {
                try {
                    String model = Build.MANUFACTURER + " " + Build.MODEL;
                    String result = postToBackend(String.format("RegistrationId=%s&Model=%s", regId, model));
                    Timber.d("Result: " + result);
                    return result;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    Timber.e(ex, "Fail");
                    Toast.makeText(getApplicationContext(), "Error Sending Registration to App Server: " + ex.getMessage(), Toast.LENGTH_LONG).show();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                Timber.d("onPostExecute result: %s", result);
            }
        }.execute();
    }

    static String postToBackend(String bodyString) throws IOException {

        final MediaType formUrlencoded = MediaType.parse("application/x-www-form-urlencoded");

        RequestBody body = RequestBody.create(formUrlencoded, bodyString);
        Request request = new Request.Builder()
                .url(API_URL)
                .post(body)
                .build();

        okhttp3.OkHttpClient mClient = new OkHttpClient();
        Response response = mClient.newCall(request).execute();
        return response.body().string();
    }
}
