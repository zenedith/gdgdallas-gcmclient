package com.mobulum.gocloudmessaging.firebase;

import android.content.Context;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mobulum.gocloudmessaging.R;

import timber.log.Timber;

public class MessagingListenerService extends FirebaseMessagingService {

    private SimpleNotificationNotifier simpleNotificationNotifier = new SimpleNotificationNotifier(this);

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Handle data payload of FCM messages.
        Timber.d("FCM From: " + remoteMessage.getFrom());
        Timber.d("FCM Message Id: " + remoteMessage.getMessageId());

        String title = getString(R.string.app_name);
        String message = "simple";

        if (remoteMessage.getNotification() != null) {
            Timber.d("FCM Notification body: " + remoteMessage.getNotification().getBody());
            Timber.d("FCM Notification title: " + remoteMessage.getNotification().getTitle());
            Timber.d("FCM Notification tag: " + remoteMessage.getNotification().getTag());

            title = remoteMessage.getNotification().getTitle();
            message = remoteMessage.getNotification().getBody();
        }

        Timber.d("FCM Data Message: " + remoteMessage.getData());

        simpleNotificationNotifier.showNotification(title, message);

        super.onMessageReceived(remoteMessage);
    }
}
