package com.mobulum.gocloudmessaging.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.NotificationCompat;

import com.mobulum.gocloudmessaging.R;
import com.mobulum.gocloudmessaging.activity.MainActivity;

public class SimpleNotificationNotifier {
    public static final String BROADCAST_ACTION = "broadcast_action";
    private Context context;

    public SimpleNotificationNotifier(Context context) {
        this.context = context;
    }

    public void showNotification(String title, String message) {
        android.support.v4.app.NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(message);

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, MainActivity.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // Id allows you to update the notification later on.
        mNotificationManager.notify(0, mBuilder.build());

        Intent broadcast = new Intent();
        broadcast.setAction(BROADCAST_ACTION);
        broadcast.putExtra("message", message);
        context.sendBroadcast(broadcast);
    }
}
