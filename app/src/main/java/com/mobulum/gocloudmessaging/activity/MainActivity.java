package com.mobulum.gocloudmessaging.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.mobulum.gocloudmessaging.R;
import com.mobulum.gocloudmessaging.firebase.SimpleNotificationNotifier;

import timber.log.Timber;

public class MainActivity extends Activity {

    private TextView textview;

//    private BroadcastReceiver receiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String message = intent.getStringExtra("message");
//            Timber.d("message: %s", message);
//            if (!TextUtils.isEmpty(message))
//                textview.setText(message);
//        }
//    };

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(getApplicationContext(), "received", Toast.LENGTH_SHORT);

            String message = intent.getStringExtra("message");
            Timber.d("message: %s", message);
            if (!TextUtils.isEmpty(message)) {
                textview.setText(message);
            }
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        GCMRegistrar.checkDevice(this);
//        GCMRegistrar.checkManifest(this);
//        final String regId = GCMRegistrar.getRegistrationId(this);
//        if (regId.equals("")) {
//            GCMRegistrar.register(this, SENDER_ID);
//        } else {
//            Log.v("com.example", "Already registered");
//        }
        textview = (TextView) findViewById(android.R.id.text1);
    }

    @Override
    protected void onResume() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(SimpleNotificationNotifier.BROADCAST_ACTION);
        registerReceiver(receiver, filter);

        Timber.d("receiver registered");
        super.onResume();
    }

    @Override
    protected void onPause() {
        unregisterReceiver(receiver);
        Timber.d("receiver unregistered");
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_unregister:
//                GCMRegistrar.unregister(this);
                return true;
            default:
                return false;
        }
    }

}
